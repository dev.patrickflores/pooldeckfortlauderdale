<?php
/**
 * Main template file for Blank WP.
 * @package blankwp
 */

get_header(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    if ( is_front_page() && is_home() ) {
        get_sidebar('bwp-herobanner');

    } else {
    // Do anything else or do nothing.
    }
    ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
        <?php the_tags(); ?>
        <?php the_post_thumbnail( 'full' ); ?>
        <?php 
        if ( has_post_format( 'video' )) {
          echo 'this is the video format';
        }
        ?>
        <?php posts_nav_link( ' &#183; ', 'previous page', 'next page' ); ?>
        <?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
        <?php wp_list_comments( $args ); ?>
        <?php wp_link_pages( $args ); ?>
        <?php comments_template( $file, $separate_comments ); ?>
        <?php comment_form(); ?>
        <?php paginate_comments_links(); ?> 
    <?php endwhile;?>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
