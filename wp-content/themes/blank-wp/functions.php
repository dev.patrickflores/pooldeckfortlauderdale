<?php
/**
 * Main function file for Blank WP.
 * @package blankwp
 */
?>

<?php
function bwp_menu() {
  register_nav_menus(array(
      'header-menu' => 'Header Menu'
    )
  );
}
add_action('init', 'bwp_menu');

function bwp_styles() {
}

add_action('wp_enqueue_scripts', 'bwp_styles');

// Add default posts and comments RSS feed links to head
  add_theme_support( 'automatic-feed-links' );

// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page.
  add_theme_support( 'post-thumbnails' );
// Setup the WordPress core custom background feature.
  add_theme_support( 'custom-background', apply_filters( 'colormag_custom_background_args', array(
    'default-color' => 'eaeaea'
  ) ) );  
/*
  * Let WordPress manage the document title.
  * By adding theme support, we declare that this theme does not use a
  * hard-coded <title> tag in the document head, and expect WordPress to
  * provide it for us.
 */
  add_theme_support('title-tag');

  // Enable support for Post Formats.
  add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link', 'gallery', 'chat', 'audio', 'status' ) );  

  // Adding excerpt option box for pages as well
  add_post_type_support( 'page', 'excerpt' );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support('html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
  ));

  // adding the WooCommerce plugin support
  add_theme_support( 'woocommerce' );
  add_theme_support( 'wc-product-gallery-zoom' );
  add_theme_support( 'wc-product-gallery-lightbox' );
  add_theme_support( 'wc-product-gallery-slider' );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support('html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
  ));

  // Adds the support for the Custom Logo introduced in WordPress 4.5
  add_theme_support( 'custom-logo',
    array(
      'flex-width' => true,
      'flex-height' => true,
    )
  );

  // Support for selective refresh widgets in Customizer
  add_theme_support( 'customize-selective-refresh-widgets' );

function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );
add_theme_support( 'custom-header' );  

if (! isset( $content_width ) ) {
  $content_width = 600;
}
add_image_size( 'full-width-horizontal', 640, 320,true );
$content_width = 600;

/*** Register widget area.***/
require get_template_directory() . '/functions/widgets.php';