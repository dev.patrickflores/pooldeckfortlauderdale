<?php
/**
 * Widgets function file for Blank WP.
 * @package blankwp
 *
 *
 * @since 1.0
 */

function bwp_widgets_init() {
		register_sidebar( array(
				'name'          => 'BWP Hero Banner', 'bwp-hero-banner',
				'id'            => 'bwp-herobanner',
				'description'   => 'Blank WP Hero Banner Widget Area.',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
		) );
		register_sidebar( array(
			'name'          => 'BWP Footer', 'bwp-footer',
			'id'            => 'bwt-footer',
			'description'   => 'Blank WP Footer Widget Area.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		) );

		register_sidebar( array(
			'name'          => 'BWP Sidebar', 'bwp-sidebar',
			'id'            => 'bwt-sidebar',
			'description'   => 'Blank WP Sidebar Widget Area.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		) );
}
add_action( 'widgets_init', 'bwp_widgets_init' );
