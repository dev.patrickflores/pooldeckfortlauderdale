<?php
/**
 * Section footer file for Blank WP.
 * @package blankwp
 */
?>
<footer id ="footer">
		<div class="footer-links">
				<div class="container">
						<div class="row">
								<?php dynamic_sidebar('bwp-footer'); ?>
						</div>
				</div>
		</div>
</footer>
