<?php
/**
 * Section header file for Blank WP.
 * @package blankwp
 */
?>
<header id="bwp-header">
		<nav id="bwp-navigation" class="navbar">
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-animations">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'nav navbar-nav navbar-right nav-menu hidden-xs','walker'=> new wp_bootstrap_navwalker()) ); ?>
			</div>
		</nav>
</header>
